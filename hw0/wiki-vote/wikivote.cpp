/*
1. Analyzing the Wikipedia voters network [27 points]

Download the Wikipedia voting network wiki-Vote.txt.gz: http://snap.stanford.edu/data/wiki-Vote.html.
Using one of the network analysis tools above, load the Wikipedia voting network. Note that
Wikipedia is a directed network. Formally, we consider the Wikipedia network as a directed
graph G = (V, E), with node set V and edge set E ⊂ V × V where (edges are ordered pairs
of nodes). An edge (a, b) ∈ E means that user a voted on user b.
To make our questions clearer, we will use the following small graph as a running example:
G small = (V small , E small ), where V small = {1, 2, 3} and E small = {(1, 2), (2, 1), (1, 3), (1, 1)}.
Compute and print out the following statistics for the wiki-Vote network:
1. The number of nodes in the network. (G small has 3 nodes.)
2. The number of nodes with a self-edge (self-loop), i.e., the number of nodes a ∈ V where
(a, a) ∈ E. (G small has 1 self-edge.)CS224W: Analysis of Networks - Problem Set 0
2
3. The number of directed edges in the network, i.e., the number of ordered pairs (a, b) ∈ E
for which a 6 = b. (G small has 3 directed edges.)
4. The number of undirected edges in the network, i.e., the number of unique unordered pairs
(a, b), a 6 = b, for which (a, b) ∈ E or (b, a) ∈ E (or both). If both (a, b) and (b, a) are edges,
this counts a single undirected edge. (G small has 2 undirected edges.)
5. The number of reciprocated edges in the network, i.e., the number of unique unordered
pairs of nodes (a, b), a 6 = b, for which (a, b) ∈ E and (b, a) ∈ E. (G small has 1 reciprocated
edge.)
6. The number of nodes of zero out-degree. (G small has 1 node with zero out-degree.)
7. The number of nodes of zero in-degree. (G small has 0 nodes with zero in-degree.)
8. The number of nodes with more than 10 outgoing edges (out-degree > 10).
9. The number of nodes with fewer than 10 incoming edges (in-degree < 10).
Each sub-question is worth 3 points.

2. Further Analyzing the Wikipedia voters network [33 points]
For this problem, we use the Wikipedia voters network. If you are using Python, you might
want to use NumPy, SciPy, and/or Matplotlib libraries.
1. (18 points) Plot the distribution of out-degrees of nodes in the network on a log-log scale.
Each data point is a pair (x, y) where x is a positive integer and y is the number of nodes
in the network with out-degree equal to x. Restrict the range of x between the minimum
and maximum out-degrees. You may filter out data points with a 0 entry. For the log-log
scale, use base 10 for both x and y axes.
2. (15 points) Compute and plot the least-square regression line for the out-degree distribution
in the log-log scale plot. Note we want to find coefficients a and b such that the function
log 10 y = a · log 10 x + b, equivalently, y = 10 b · x a , best fits the out-degree distribution.
What are the coefficients a and b? For this part, you might want to use the method called
polyfit in NumPy with deg parameter equal to 1.
*/

#include <algorithm>
#include <chrono>
#include <cmath>
#include <deque>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "matplotlibcpp.h"
namespace plt = matplotlibcpp;

template <class T>
struct Graph {
  using NodeList = std::deque<T>;
  using Container = std::unordered_map<T, NodeList>;

  explicit Graph(const std::string & fname) {
    load(fname);
  }

  void load(const std::string & fname);

  std::size_t vertices() const { return vertices_; }
  std::size_t edges() const { return edges_; }
  std::size_t self_loops() const { return self_loops_; }

  Container outgoing() const { return outgoing_; }

  std::size_t count_directed_edges() const {
    auto acc = [](const auto & g, long a, long b) {
      if (g.find(b) == std::end(g))
        return 1;
      const auto & bb = g.at(b);
      return std::find(std::begin(bb), std::end(bb), a) != std::end(bb) ? 2 : 1;
    };
    return count_edges(acc);
  }

  std::size_t count_undirected_edges() const {
    auto acc = [](const auto & g, long a, long b) { return 1; };
    return count_edges(acc);
  }

  std::size_t count_reciprocated_edges() const {
    auto acc = [](const auto & g, long a, long b) {
      if (g.find(b) == std::end(g))
        return 0;
      const auto & bb = g.at(b);
      return std::find(std::begin(bb), std::end(bb), a) != std::end(bb) ? 1 : 0;
    };
    return count_edges(acc);
  }

  std::size_t count_zero_out_degree_nodes() const {
    auto acc = [](const auto & nodes) { return nodes.empty() ? 1 : 0; };
    return count_out_degree_nodes(acc);
  }

  std::size_t count_more10_out_degree_nodes() const {
    auto acc = [](const auto & nodes) { return nodes.size() > 10 ? 1 : 0; };
    return count_out_degree_nodes(acc);
  }

  std::size_t count_zero_in_degree_nodes() const {
    auto acc = [](const auto & nodes) { return nodes.empty() ? 1 : 0; };
    return count_in_degree_nodes(acc);
  }

  std::size_t count_more10_in_degree_nodes() const {
    auto acc = [](const auto & nodes) { return nodes.size() > 10 ? 1 : 0; };
    return count_in_degree_nodes(acc);
  }

private:
  Container outgoing_;
  Container incoming_;
  std::size_t vertices_{0};
  std::size_t edges_{0};
  std::size_t self_loops_{0};

  struct edge_hash {
    template <typename T1, typename T2>
    std::size_t operator()(const std::pair<T1, T2> & p) const {
      return std::hash<T1>()(p.first) ^ std::hash<T2>()(p.second);
    }
  };

  std::size_t count_edges(std::function<std::size_t(const Container &, T, T)> accumulator) const;

  std::size_t count_out_degree_nodes(std::function<std::size_t(const NodeList &)> accumulator) const;

  std::size_t count_in_degree_nodes(std::function<std::size_t(const NodeList &)> accumulator) const;
};

template <class T>
void Graph<T>::load(const std::string & fname) {
  std::ifstream fs(fname.c_str());
  std::string str;
  while (std::getline(fs, str)) {
    if (str[0] == '#') continue;
    std::istringstream iss(str);
    T from, to;
    iss >> from >> to;
    if (from == to) ++self_loops_;
    if (outgoing_.find(from) == std::end(outgoing_)) {
      outgoing_[from] = {};
    }
    if (outgoing_.find(to) == std::end(outgoing_)) {
      outgoing_[to] = {};
    }
    outgoing_[from].push_back(to);
    if (incoming_.find(from) == std::end(incoming_)) {
      incoming_[from] = {};
    }
    if (incoming_.find(to) == std::end(incoming_)) {
      incoming_[to] = {};
    }
    incoming_[to].push_back(from);
    ++edges_;
  }
  vertices_ = outgoing_.size();
}

template <class T>
std::size_t Graph<T>::count_edges(std::function<std::size_t(const Container &, T, T)> accumulator) const {
  std::size_t result{0};
  std::unordered_set<std::pair<T, T>, edge_hash> checked_edges;
  for (const auto & entry : outgoing_) {
    auto a = entry.first;
    for (const auto b : entry.second) {
      std::pair p{std::min(a, b), std::max(a, b)};
      if (a != b && checked_edges.find(p) == std::end(checked_edges)) {
        result += accumulator(outgoing_, a, b);
      }
      checked_edges.emplace(p);
    }
  }
  return result;
}

template <class T>
std::size_t Graph<T>::count_out_degree_nodes(std::function<std::size_t(const NodeList &)> accumulator) const {
  std::size_t result{0};
  for (const auto & entry : outgoing_) {
    result += accumulator(entry.second);
  }
  return result;
}

template <class T>
std::size_t Graph<T>::count_in_degree_nodes(std::function<std::size_t(const NodeList &)> accumulator) const {
  std::size_t result{0};
  for (const auto & entry : incoming_) {
    result += accumulator(entry.second);
  }
  return result;
}

template <class T>
void distribution_plot(const Graph<T> & g) {
  const auto & data = g.outgoing();
  std::size_t n{data.size()};
  std::vector<T> keys(n);
  std::transform(data.begin(), data.end(), keys.begin(), [](auto p){ return p.first; });
  std::sort(keys.begin(), keys.end());
  std::vector<double> x(n), y(n);
  double log10 = log(10);
  std::size_t i{0};
  for (const auto & key : keys) {
    x[i] = log(key) / log10;
    y[i] = log(data.at(key).size()) / log10;
    ++i;
  }
  plt::figure_size(1200, 780);
  plt::plot(x, y);
  plt::save("./distribution.png");
}

template <class T>
void polyfit(const Graph<T> & g, std::size_t deg = 1) {
  const auto & data = g.outgoing();
  std::size_t n{data.size()};
  std::vector<T> keys(n);
  std::transform(data.begin(), data.end(), keys.begin(), [](auto p){ return p.first; });
  std::sort(keys.begin(), keys.end());
  std::vector<double> x(n), y(n);
  double log10 = log(10);
  std::size_t i{0}, j, k;
  for (const auto & key : keys) {
    x[i] = log(key) / log10;
    y[i] = log(data.at(key).size()) / log10;
    ++i;
  }
  std::vector<double> X(2 * deg + 1);
  for (i = 0; i < X.size(); ++i) {
    X[i] = 0.0;
    for (j = 0; j < n; ++j) {
      X[i] += pow(x[j], i);
    }
  }
  std::vector<double> a(deg + 1);
  std::vector<std::vector<double>> B(deg + 1, std::vector<double>(deg + 2, 0.0));
  for (i = 0; i <= deg; ++i) {
    for (j = 0; j <= deg; ++j) {
      B[i][j] = X[i + j];
    }
  }
  std::vector<double> Y(deg + 1);
  for (i = 0; i < deg + 1; ++i) {
    Y[i] = 0.0;
    for (j = 0; j < n; ++j) {
      Y[i] += pow(x[j], i) * y[j];
    }
  }
  for (i = 0; i <= deg; ++i) {
    B[i][n + 1] = Y[i];
  }
  deg++;
  for (i = 0; i < deg; ++i) {
    for (k = i + 1; k < deg; ++k) {
      if (B[i][i] < B[k][i]) {
        double tmp;
        for (j = 0; j <= deg; ++j) {
          tmp = B[i][j];
          B[i][j] = B[k][j];
          B[k][j] = tmp;
        }
      }
    }
  }
  for (i = 0; i < deg - 1; ++i) {
    for (k = i + 1; k < deg; ++k) {
      double t = B[k][i] / B[i][i];
      for (j = 0; j <= n; ++j) {
        B[k][j] -= t * B[i][j];
      }
    }
  }
  for (int z = deg - 1; z >= 0; --z) {
    a[z] = B[i][n];
    for (j = 0; j < deg; ++j) {
      if (static_cast<int>(j) != z) {
        a[z] -= B[z][j] * a[j];
      }
    }
    a[z] /= B[i][i];
  }

  for (i = 0; i < a.size(); ++i)
    std::cout << (i == 0 ? " " : " + ") << a[i] <<" * x^" << i;
  std::cout << std::endl;
}

int main() {
  auto now = std::chrono::high_resolution_clock::now();
  Graph<long> g{"data/wiki-Vote.txt"};
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
    std::chrono::high_resolution_clock::now() - now).count();
  std::cout << "Graph loaded in " << elapsed << " ms" << std::endl;
  std::cout << g.vertices() << " vertices and " << g.edges() << " edges" << std::endl;
  std::cout << g.self_loops() << " node(s) with self-edge" << std::endl;
  std::cout << g.count_directed_edges() << " directed edge(s) in the network" << std::endl;
  std::cout << g.count_undirected_edges() << " undirected edge(s) in the network" << std::endl;
  std::cout << g.count_reciprocated_edges() << " reciprocated edge(s) in the network" << std::endl;
  std::cout << g.count_zero_out_degree_nodes() << " node(s) of zero out-degree" << std::endl;
  std::cout << g.count_zero_in_degree_nodes() << " node(s) of zero in-degree" << std::endl;
  std::cout << g.count_more10_out_degree_nodes() << " node(s) with more than 10 outgoing edges" << std::endl;
  std::cout << g.count_more10_in_degree_nodes() << " node(s) with more than 10 ingoing edges" << std::endl;

  distribution_plot(g);
//  polyfit(g);
}
